class Pipeline {
  final int id;
  final int projectId;
  final String status;
  final DateTime createdAt;
  final String ref;
  final String refSha;

  Pipeline({
    required this.id,
    required this.status,
    required this.createdAt,
    required this.ref,
    required this.projectId,
    required this.refSha,
  });
}
