class Badge {
  final int _id;
  final String _name;
  final String _imageUrl;

  Badge(this._id, this._name, this._imageUrl);

  int get id => _id;
  String get name => _name;
  String get imageUrl => _imageUrl;
}
