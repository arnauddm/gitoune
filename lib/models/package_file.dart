class PackageFile {
  final int id;
  final int packageId;
  final DateTime createdAt;
  final String fileName;
  final int size;
  final String fileMd5;
  final String fileSha1;
  final String fileSha256;

  PackageFile({
    required this.id,
    required this.packageId,
    required this.createdAt,
    required this.fileMd5,
    required this.fileName,
    required this.fileSha1,
    required this.fileSha256,
    required this.size,
  });
}
