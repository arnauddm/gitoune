class Job {
  final int id;
  final String userAuthor;
  final double duration;
  final List<dynamic> tags;
  final String name;
  final String stage;
  final String status;
  final List<dynamic> artifacts;
  final String ref;
  final int projectId;
  final bool allowFailure;

  Job(
      {required this.id,
      required this.userAuthor,
      required this.duration,
      required this.name,
      required this.stage,
      required this.tags,
      required this.status,
      required this.artifacts,
      required this.ref,
      required this.projectId,
      required this.allowFailure});
}
