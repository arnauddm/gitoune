class Project {
  final int id;
  final String groupName;
  final String name;
  final String description;
  final String webUrl;
  final String readMeUrl;
  final List<dynamic> tags;
  final int startCount;
  final int forkCount;
  final String nameWithNamespace;

  Project({
    required this.id,
    required this.groupName,
    required this.name,
    required this.description,
    required this.webUrl,
    required this.readMeUrl,
    required this.tags,
    required this.startCount,
    required this.forkCount,
    required this.nameWithNamespace,
  });
}
