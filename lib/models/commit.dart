class Commit {
  final String id;
  final String shortId;
  final String title;
  final String message;
  final String authorEmail;
  final String authorName;
  final String committerName;
  final DateTime createdAt;
  final DateTime committedDate;
  final DateTime authoredTime;

  Commit({
    required this.id,
    required this.shortId,
    required this.title,
    required this.message,
    required this.authorEmail,
    required this.createdAt,
    required this.authorName,
    required this.authoredTime,
    required this.committedDate,
    required this.committerName,
  });
}
