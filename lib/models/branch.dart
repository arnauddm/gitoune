import 'dart:core';

import 'package:gitoune/models/commit.dart';

class Branch {
  final String name;
  final bool isMerged;
  final bool isProtected;
  final bool isDefaultBranch;
  final bool developersCanPush;
  final bool developersCanMerge;
  final bool canPush;
  final String webUrl;
  final Commit commit;

  Branch({
    required this.name,
    required this.isMerged,
    required this.canPush,
    required this.commit,
    required this.developersCanMerge,
    required this.developersCanPush,
    required this.isProtected,
    required this.webUrl,
    required this.isDefaultBranch,
  });
}
