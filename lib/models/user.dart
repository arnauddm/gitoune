class User {
  final String username;
  final String name;
  final String email;
  final String avatarUrl;

  User(this.username, this.name, this.email, this.avatarUrl);
}
