class Package {
  final int id;
  final String name;
  final String version;
  final String type;
  final DateTime createdAt;

  Package({
    required this.id,
    required this.name,
    required this.version,
    required this.type,
    required this.createdAt,
  });
}
