import 'package:flutter/material.dart';

class ColorConstants {
  static const Color primary = Color.fromRGBO(44, 55, 86, 1);
  static const Color secondary = Color.fromRGBO(110, 150, 244, 1);

  static const Color pipelinePending = Color(0xffffbe76);
  static const Color pipelineFailed = Color(0xffff7979);
  static const Color pipelineSuccess = Color(0xffbadc58);
  static const Color pipelineRunning = Color(0xff686de0);
  static const Color pipelineDefault = Color(0xffc7ecee);
  static const Color pipelineAllowFailure = Color(0xfff0932b);

  static const Color projectTag = Color(0xff22a6b3);
}
