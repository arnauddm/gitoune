// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/app_layout.dart';
import 'package:gitoune/screens/branch/branches_list_screen.dart';
import 'package:gitoune/screens/login/login_screen.dart';
import 'package:gitoune/screens/package/package_screen.dart';
import 'package:gitoune/screens/pipeline/pipelines_screen.dart';
import 'package:gitoune/screens/project/pipeline_resume.dart';
import 'package:gitoune/screens/readme/readme_screen.dart';
import 'package:gitoune/screens/readme/readme_viewer.dart';
import 'package:gitoune/screens/project/statistics.dart';
import 'package:gitoune/screens/project/tags_view.dart';
import 'package:gitoune/screens/widgets/card.dart';
import 'package:gitoune/screens/widgets/title.dart';
import 'package:google_fonts/google_fonts.dart';

class ProjectScreen extends StatelessWidget {
  final Project project;

  const ProjectScreen(this.project, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScreenTitle(
            title: project.name,
            subTitle: project.groupName,
          ),
          const Spacer(),
          Wrap(
            children: [
              TagsList(project.tags),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              StartCount(project.startCount),
              const SizedBox(width: 10),
              ForkCount(project.forkCount),
            ],
          ),
          const Spacer(),
          const Divider(
            color: ColorConstants.primary,
            thickness: 3,
          ),
          //BadgesList(project: project),
          //const SizedBox(height: 30),
          if (project.description != "")
            CardItem(
              title: "Description",
              content: Text(
                project.description,
                style: const TextStyle(fontSize: 15),
              ),
            ),
          const Spacer(),
          CardItem(
              title: "Latest pipeline",
              content: PipelineResume(project: project)),
          const Spacer(),
          GridView.count(
            crossAxisCount: 2,
            mainAxisSpacing: 50.0,
            crossAxisSpacing: 50.0,
            childAspectRatio: 1.0,
            padding: const EdgeInsets.all(4.0),
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              ProjectStatisticShortcut(
                icon: Icons.auto_stories,
                title: "Read me",
                callback: ReadMeScreen(project: project),
              ),
              ProjectStatisticShortcut(
                icon: Icons.card_travel,
                title: "Packages",
                callback: PackageScreen(project: project),
              ),
              ProjectStatisticShortcut(
                icon: Icons.checklist,
                title: "Pipeline",
                callback: PipelinesListScreen(project: project),
              ),
              ProjectStatisticShortcut(
                icon: Icons.alt_route,
                title: "Branches",
                callback: BranchesListScreen(project: project),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class Spacer extends StatelessWidget {
  const Spacer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 30);
  }
}

class ProjectStatisticShortcut extends StatelessWidget {
  final IconData icon;
  final String title;
  final StatelessWidget callback;

  const ProjectStatisticShortcut(
      {Key? key,
      required this.icon,
      required this.title,
      required this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => callback));
      },
      child: Container(
        //width: MediaQuery.of(context).size.width * .3,
        //height: MediaQuery.of(context).size.width * .3,
        decoration: BoxDecoration(
          color: ColorConstants.primary,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(icon, color: Colors.white, size: 40),
            const SizedBox(height: 5),
            Text(
              title,
              style: const TextStyle(color: Colors.white, fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
