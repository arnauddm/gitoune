import 'package:flutter/material.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/project.dart';

class BadgesList extends StatefulWidget {
  final Project project;

  const BadgesList({Key? key, required this.project}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BadgesListState();
  }
}

class _BadgesListState extends State<BadgesList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Gitlab().getProjectBadge(widget.project.id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return Expanded(
            child: ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var url = snapshot.data[index].imageUrl;
                return Image.network(
                    url); //return Image.network(snapshot.data[index].imageUrl);
              },
            ),
          );
        });
  }
}
