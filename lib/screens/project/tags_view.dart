import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';

class TagsList extends StatelessWidget {
  final List<dynamic> _tags;

  const TagsList(this._tags, {Key? key}) : super(key: key);

  List<Widget> buildTags() {
    List<Widget> tags = [];

    for (String t in _tags) {
      tags.add(Tag(t));
    }
    return tags;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: buildTags(),
    );
  }
}

class Tag extends StatelessWidget {
  final String value;

  const Tag(this.value, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(2),
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(
          color: ColorConstants.projectTag,
          borderRadius: BorderRadius.circular(20)),
      child: Text(value),
    );
  }
}
