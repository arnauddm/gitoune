import 'package:flutter/material.dart';

class StartCount extends StatelessWidget {
  final int stars;

  const StartCount(this.stars, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProjectStatisticItem(
      stars.toString(),
      Icons.star_rounded,
    );
  }
}

class ForkCount extends StatelessWidget {
  final int forks;

  const ForkCount(this.forks, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProjectStatisticItem(
      forks.toString(),
      Icons.account_tree_rounded,
    );
  }
}

class ProjectStatisticItem extends StatelessWidget {
  final String value;
  final IconData icon;

  const ProjectStatisticItem(this.value, this.icon, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(value),
        const SizedBox(width: 2),
        Icon(icon),
      ],
    );
  }
}
