import 'package:flutter/material.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/pipeline/pipelines_screen.dart';
import 'package:gitoune/screens/pipeline/pipelines_tile.dart';

class PipelineResume extends StatefulWidget {
  final Project project;

  const PipelineResume({Key? key, required this.project}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PipelineResumeState();
  }
}

class _PipelineResumeState extends State<PipelineResume> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future:
            Gitlab().getPipelinesForProject(widget.project.id, itemCount: 1),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return Row(
            children: [
              Expanded(
                child: snapshot.data.length == 0
                    ? const Text("No pipeline found for this project !")
                    : PipelineCard(snapshot.data[0]!),
              ),
              if (snapshot.data.length > 0)
                IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PipelinesListScreen(project: widget.project)));
                  },
                  icon: const Icon(Icons.read_more, size: 40),
                ),
            ],
          );
        });
  }
}
