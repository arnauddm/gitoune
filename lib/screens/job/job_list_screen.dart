import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/pipeline.dart';
import 'package:gitoune/screens/app_layout.dart';
import 'package:gitoune/screens/widgets/title.dart';
import 'job_tile.dart';

class JobsListScreen extends StatelessWidget {
  final Pipeline pipeline;

  const JobsListScreen({Key? key, required this.pipeline}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScreenTitle(
              title: "Jobs", subTitle: "Pipeline " + pipeline.id.toString()),
          const SizedBox(height: 50),
          PipelineCommitDetail(pipeline: pipeline),
          Column(
            children: [
              JobDetailsTiles(pipeline: pipeline),
            ],
          ),
        ],
      ),
    );
  }
}

class PipelineCommitDetail extends StatefulWidget {
  final Pipeline pipeline;

  const PipelineCommitDetail({Key? key, required this.pipeline})
      : super(key: key);

  @override
  State<PipelineCommitDetail> createState() => _PipelineCommitDetailState();
}

class _PipelineCommitDetailState extends State<PipelineCommitDetail> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Gitlab()
            .getCommit(widget.pipeline.projectId, widget.pipeline.refSha),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return const Padding(
              padding: EdgeInsets.all(50),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }

          return Column(
            children: [
              Text(
                snapshot.data.title,
                style: const TextStyle(
                    fontSize: 22, color: ColorConstants.primary),
              ),
              const SizedBox(height: 20),
              if ((snapshot.data.title.compareTo(snapshot.data.message))
                      .abs() !=
                  (snapshot.data.title.length - snapshot.data.message.length)
                      .abs())
                Text(
                  snapshot.data.message,
                  style: const TextStyle(fontSize: 17),
                ),
            ],
          );
        });
  }
}
