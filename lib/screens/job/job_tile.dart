import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/gitlab/utils/resources_switcher.dart';
import 'package:gitoune/models/job.dart';
import 'package:gitoune/models/pipeline.dart';
import 'package:gitoune/screens/job/artifact_tile.dart';
import 'package:gitoune/screens/widgets/card.dart';

class JobDetailsTiles extends StatefulWidget {
  final Pipeline pipeline;

  const JobDetailsTiles({Key? key, required this.pipeline}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _JobDetailsTiles();
  }
}

class _JobDetailsTiles extends State<JobDetailsTiles> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Gitlab().getJobs(widget.pipeline.projectId, widget.pipeline.id),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return ListView.builder(
          shrinkWrap: true,
          physics: const ScrollPhysics(),
          itemCount: snapshot.data.keys.length,
          itemBuilder: (BuildContext context, int index) {
            final MapEntry<String, List<Job>> mapEntry =
                snapshot.data.entries.elementAt(index);

            return Column(
              children: [
                const SizedBox(height: 20),
                CardItem(
                  title: mapEntry.key,
                  content: Column(
                    children: [
                      ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: mapEntry.value.length,
                        itemBuilder: (BuildContext subContext, int subIndex) {
                          return Column(
                            children: [
                              JobTile(
                                job: mapEntry.value[subIndex],
                                projectId: widget.pipeline.projectId,
                              ),
                              if (subIndex < mapEntry.value.length - 1)
                                const Divider(
                                    thickness: 2,
                                    color: ColorConstants.secondary),
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}

class JobTile extends StatelessWidget {
  final int projectId;
  final Job job;

  const JobTile({Key? key, required this.job, required this.projectId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ResourcesSwitcher.getColor(job.status,
            allowFailure: job.allowFailure),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: ExpansionTile(
          title: Row(
            children: [
              Icon(ResourcesSwitcher.getIcon(job.status,
                  allowFailure: job.allowFailure)),
              const SizedBox(width: 10),
              Expanded(
                child: Text(
                  job.name,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              const SizedBox(width: 10),
              Row(
                children: [
                  const Icon(Icons.timer),
                  Text(job.duration.truncate().toString() + " s.")
                ],
              ),
            ],
          ),
          children: [
            Column(
              children: [
                ArtifactTile(
                  job: job,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
