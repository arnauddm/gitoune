import 'package:flutter/material.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/job.dart';

class ArtifactTile extends StatefulWidget {
  final Job job;

  const ArtifactTile({Key? key, required this.job}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ArtifactTileState();
  }
}

class _ArtifactTileState extends State<ArtifactTile> {
  @override
  Widget build(BuildContext context) {
    if (widget.job.artifacts.isNotEmpty) {
      return ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: widget.job.artifacts.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () async {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text("Download artifact"),
                        content: DownloadArtifact(
                            artifactName: widget.job.artifacts[index],
                            jobName: widget.job.name,
                            projectId: widget.job.projectId,
                            ref: widget.job.ref),
                      );
                    });
              },
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    const Icon(Icons.download, size: 30),
                    Expanded(
                      child: Text(
                        widget.job.artifacts[index],
                        style: const TextStyle(fontSize: 17),
                      ),
                    )
                  ],
                ),
              ),
            );
          });
    } else {
      return const Text(
        "No artifact found for this job ",
        style: TextStyle(fontSize: 20),
      );
    }
  }
}

class DownloadArtifact extends StatefulWidget {
  final int projectId;
  final String ref;
  final String artifactName;
  final String jobName;

  const DownloadArtifact(
      {Key? key,
      required this.projectId,
      required this.ref,
      required this.artifactName,
      required this.jobName})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DownloadArtifactState();
  }
}

class _DownloadArtifactState extends State<DownloadArtifact> {
  String? outputMessage;
  bool sent = false;

  @override
  Widget build(BuildContext context) {
    if (!sent) {
      sent = true;

      Gitlab()
          .downloadArtifact(
              widget.projectId, widget.ref, widget.artifactName, widget.jobName)
          .then((msg) => {
                setState(() {
                  outputMessage = msg;
                })
              });
    }

    if (outputMessage == null) {
      return const Center(child: CircularProgressIndicator());
    }

    return Center(
      child: Text(outputMessage!),
    );
  }
}
