import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:google_fonts/google_fonts.dart';

class ScreenTitle extends StatelessWidget {
  final String? subTitle;
  final String title;

  const ScreenTitle({Key? key, required this.title, this.subTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      if (subTitle != null)
        Text(
          subTitle!,
          style: GoogleFonts.comicNeue(
              fontSize: 15, color: ColorConstants.primary),
        ),
      Text(
        title,
        style:
            GoogleFonts.bebasNeue(fontSize: 70, color: ColorConstants.primary),
      )
    ]);
  }
}
