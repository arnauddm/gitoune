import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';

// ignore: must_be_immutable
class CardItem extends StatefulWidget {
  final String title;
  final Widget content;

  Color color = ColorConstants.primary;

  CardItem(
      {Key? key,
      this.color = ColorConstants.primary,
      required this.title,
      required this.content})
      : super(key: key);

  @override
  State<CardItem> createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
              color: widget.color,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            child: Text(widget.title,
                style: const TextStyle(color: Colors.white, fontSize: 22)),
          ),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(color: widget.color),
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            child: widget.content,
          ),
        ],
      ),
    );
  }
}
