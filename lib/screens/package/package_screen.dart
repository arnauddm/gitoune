import 'package:flutter/material.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/package.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/app_layout.dart';
import 'package:gitoune/screens/package/package_tile.dart';
import 'package:gitoune/screens/widgets/card.dart';
import 'package:gitoune/screens/widgets/title.dart';

class PackageScreen extends StatelessWidget {
  final Project project;

  const PackageScreen({Key? key, required this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScreenTitle(
            title: "Packages",
            subTitle: project.nameWithNamespace,
          ),
          PackagesList(project: project)
        ],
      ),
    );
  }
}

class PackagesList extends StatefulWidget {
  final Project project;

  const PackagesList({Key? key, required this.project}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PackagesListState();
  }
}

class _PackagesListState extends State<PackagesList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Gitlab().getProjectPackages(widget.project.id),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: snapshot.data.keys.length,
          itemBuilder: (BuildContext context, int index) {
            final MapEntry<String, List<Package>> mapEntry =
                snapshot.data.entries.elementAt(index);
            final String version = mapEntry.key;
            final List<Package> packages = mapEntry.value;

            return CardItem(
              title: "Version " + version,
              content: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: packages.length,
                itemBuilder: (BuildContext subContext, int subIndex) {
                  return ExpansionTile(
                    title: Text(
                      packages[subIndex].name,
                      style: const TextStyle(fontSize: 22),
                    ),
                    children: [
                      PackageTile(
                          project: widget.project,
                          packageId: packages[index].id)
                    ],
                  );
                },
              ),
            );
          },
        );
      },
    );
  }
}
