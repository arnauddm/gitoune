import 'package:flutter/material.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/package_file.dart';
import 'package:gitoune/models/project.dart';

class PackageTile extends StatefulWidget {
  final Project project;
  final int packageId;

  const PackageTile({Key? key, required this.project, required this.packageId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PackageTileState();
  }
}

class _PackageTileState extends State<PackageTile> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Gitlab().getPackageFiles(widget.project.id, widget.packageId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null) {
          return const Center(child: CircularProgressIndicator());
        }

        List<PackageFile> files = snapshot.data;

        return ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: files.length,
          itemBuilder: (BuildContext subContext, int index) {
            return InkWell(
              onTap: () {
                Gitlab()
                    .downloadPackage(widget.project.webUrl, files[index].id);
              },
              child: Row(
                children: [
                  const Icon(Icons.chevron_right_rounded),
                  Expanded(
                    child: Text(
                      files[index].fileName,
                      style: const TextStyle(fontSize: 17),
                    ),
                  ),
                  const Icon(Icons.storage_rounded),
                  Text(files[index].size.toString()),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
