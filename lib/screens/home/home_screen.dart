import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/models/user.dart';
import 'package:gitoune/screens/app_layout.dart';
import 'package:gitoune/screens/home/project_tile.dart';
import 'package:gitoune/screens/home/user_tile.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeScreen extends StatelessWidget {
  final User user;

  const HomeScreen({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UserInfoTile(user: user),
          const SizedBox(height: 50),
          Text(
            "Projects",
            style: GoogleFonts.bebasNeue(
                color: ColorConstants.primary, fontSize: 30),
          ),
          const ProjectList(),
        ],
      ),
    );
  }
}
