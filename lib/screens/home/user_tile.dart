import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/models/user.dart';
import 'package:google_fonts/google_fonts.dart';

class UserInfoTile extends StatelessWidget {
  final User user;

  const UserInfoTile({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UserTile(user: user);
  }
}

class UserTile extends StatelessWidget {
  final User user;

  const UserTile({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      //padding: const EdgeInsets.all(40),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.white10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            user.name,
            style: GoogleFonts.bebasNeue(
                fontSize: 70, color: ColorConstants.primary),
          ),
          Text(
            "Welcome Back !",
            style: GoogleFonts.comicNeue(fontSize: 40, color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
