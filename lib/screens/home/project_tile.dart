// ignore_for_file: unused_import
import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/project/project_screen.dart';

class ProjectList extends StatefulWidget {
  const ProjectList({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProjectListStates();
  }
}

class _ProjectListStates extends State<ProjectList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Gitlab().getProjects(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(children: [
              ProjectCard(project: snapshot.data[index]),
              const SizedBox(height: 10)
            ]);
          },
        );
      },
    );
  }
}

class ProjectCard extends StatelessWidget {
  final Project project;

  const ProjectCard({Key? key, required this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ProjectScreen(project)));
      },
      child: Container(
        padding: const EdgeInsets.all(30),
        width: double.infinity,
        decoration: const BoxDecoration(
          color: ColorConstants.primary,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              topRight: Radius.circular(80),
              bottomRight: Radius.circular(80)),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    project.name,
                    style: const TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  const SizedBox(height: 10),
                  if (project.groupName != "")
                    Text(
                      project.groupName,
                      style: const TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  if (project.groupName != "") const SizedBox(height: 10),
                  Text(
                    project.description.length <= 30
                        ? project.description
                        : project.description.replaceRange(
                            30, project.description.length, "..."),
                    style: const TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ],
              ),
            ),
            const Icon(Icons.chevron_right, color: Colors.white, size: 50)
          ],
        ),
      ),
    );
  }
}
