import 'package:flutter/material.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/pipeline/pipelines_tile.dart';
import 'package:gitoune/screens/widgets/title.dart';

import '../app_layout.dart';

class PipelinesListScreen extends StatelessWidget {
  final Project project;

  const PipelinesListScreen({Key? key, required this.project})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScreenTitle(
            title: "Pipelines",
            subTitle: project.nameWithNamespace,
          ),
          PipelineTiles(project),
        ],
      ),
    );
  }
}
