// ignore_for_file: unnecessary_import

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/gitlab/utils/resources_switcher.dart';
import 'package:gitoune/models/pipeline.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/job/job_list_screen.dart';
import 'package:intl/intl.dart';

class PipelineTiles extends StatefulWidget {
  final Project project;

  const PipelineTiles(this.project, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PipelineTileStates();
  }
}

class _PipelineTileStates extends State<PipelineTiles> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Gitlab().getPipelinesForProject(widget.project.id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(children: [
                PipelineCard(snapshot.data[index]),
                const SizedBox(height: 10)
              ]);
            },
          );
        });
  }
}

class PipelineCard extends StatelessWidget {
  final Pipeline pipeline;

  const PipelineCard(this.pipeline, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => JobsListScreen(
              pipeline: pipeline,
            ),
          ),
        );
      },
      child: Container(
        padding:
            const EdgeInsets.only(top: 10, right: 10, left: 30, bottom: 10),
        decoration: BoxDecoration(
          color: ResourcesSwitcher.getColor(pipeline.status),
          borderRadius: BorderRadius.circular(30),
        ),
        width: double.infinity,
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pipeline.ref,
                    style: const TextStyle(fontSize: 20),
                  ),
                  Text(
                    DateFormat('yyyy-MM-dd – kk:mm').format(pipeline.createdAt),
                  ),
                ],
              ),
            ),
            Icon(
              ResourcesSwitcher.getIcon(pipeline.status),
              color: Colors.white,
              size: 35,
            )
          ],
        ),
      ),
    );
  }
}
