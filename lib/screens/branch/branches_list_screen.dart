import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/branch.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/app_layout.dart';
import 'package:gitoune/screens/widgets/title.dart';

class BranchesListScreen extends StatelessWidget {
  final Project project;

  const BranchesListScreen({Key? key, required this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
        content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ScreenTitle(
          title: "Branches",
          subTitle: project.nameWithNamespace,
        ),
        BranchesList(project: project),
      ],
    ));
  }
}

class BranchesList extends StatefulWidget {
  final Project project;

  const BranchesList({Key? key, required this.project}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BranchesListState();
  }
}

class _BranchesListState extends State<BranchesList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Gitlab().getBranches(widget.project.id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return ListView.builder(
            itemCount: snapshot.data.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext subContext, int index) {
              return BranchCard(
                branch: snapshot.data[index],
              );
            },
          );
        });
  }
}

class BranchCard extends StatelessWidget {
  final Branch branch;

  const BranchCard({Key? key, required this.branch}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      decoration: BoxDecoration(
        border: Border.all(color: ColorConstants.primary),
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          branch.isProtected ? const Icon(Icons.lock) : const Icon(null),
          branch.isMerged ? const Icon(Icons.merge_type) : const Icon(null),
          Expanded(
            child: Text(
              branch.name,
              style:
                  const TextStyle(fontSize: 18, color: ColorConstants.primary),
            ),
          ),
          Container(
            height: 50,
            width: 70,
            decoration: const BoxDecoration(
              color: ColorConstants.primary,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
            ),
            child: const Icon(Icons.east, color: Colors.white),
          ),
        ],
      ),
    );
  }
}
