import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/models/user.dart';
import 'package:gitoune/screens/home/home_screen.dart';

class LoginForm extends StatelessWidget {
  final TextEditingController tokenController;
  final TextEditingController urlController;

  const LoginForm(
      {Key? key, required this.tokenController, required this.urlController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        FormInput(
            controller: tokenController,
            hintLabel: "Private access token",
            isPassword: true),
        const SizedBox(height: 20),
        FormInput(
            controller: urlController,
            hintLabel: "Gitlab API url",
            isPassword: false),
        TokenValidationButton(
          tokenController: tokenController,
          urlController: urlController,
        ),
      ],
    );
  }
}

class FormInput extends StatelessWidget {
  final TextEditingController controller;
  final String hintLabel;
  final bool isPassword;

  const FormInput(
      {Key? key,
      required this.controller,
      required this.hintLabel,
      required this.isPassword})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: ColorConstants.primary.withOpacity(.5),
            blurRadius: 20,
            offset: const Offset(0, 10),
          ),
        ],
      ),
      child: TextField(
        obscureText: isPassword,
        controller: controller,
        decoration: const InputDecoration(
          hintText: "Gitlab API Url",
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class TokenValidationButton extends StatefulWidget {
  final TextEditingController tokenController;
  final TextEditingController urlController;

  const TokenValidationButton(
      {Key? key, required this.tokenController, required this.urlController})
      : super(key: key);

  @override
  State<TokenValidationButton> createState() => _TokenValidationButtonState();
}

enum LoginStatus { none, waiting, notFound }

class _TokenValidationButtonState extends State<TokenValidationButton> {
  LoginStatus status = LoginStatus.none;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextButton(
          onPressed: () async {
            setState(() {
              status = LoginStatus.waiting;
            });

            final String token = widget.tokenController.text;
            final String url = widget.urlController.text;

            Gitlab.configureBaseUrl = url;

            if (token != "") {
              User? user = await Gitlab().getUser(token);

              if (user != null) {
                Gitlab.login = token;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => HomeScreen(user: user)));
              } else {
                setState(() {
                  status = LoginStatus.notFound;
                });
              }
            }
          },
          child: Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: ColorConstants.primary,
              borderRadius: BorderRadius.circular(20),
            ),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  "Next",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
                SizedBox(width: 20),
                Icon(
                  Icons.chevron_right,
                  color: Colors.white,
                  size: 27,
                ),
              ],
            ),
          ),
        ),
        LoginStatusWidget(status: status)
      ],
    );
  }
}

class LoginStatusWidget extends StatelessWidget {
  final LoginStatus status;

  const LoginStatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (status) {
      case LoginStatus.waiting:
        return const Center(
          child: CircularProgressIndicator(),
        );
      case LoginStatus.notFound:
        return const Center(child: Text("User not found with the given token"));
      default:
        return Container();
    }
  }
}
