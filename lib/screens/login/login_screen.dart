import 'package:flutter/material.dart';
import 'package:gitoune/constants/color_constants.dart';
import 'package:gitoune/constants/string_constants.dart';
import 'package:gitoune/gitlab/gitlab.dart';
import 'package:gitoune/screens/login/login_form.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final tokenController =
      TextEditingController(text: "glpat-fG-79QRH7wKPhGNcCTNn");

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: ColorConstants.primary),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(height: 150),
                Text(
                  "Gitoune",
                  style: GoogleFonts.bangers(fontSize: 60, color: Colors.white),
                ),
                const SizedBox(height: 20),
                Text(
                  "Track your Git everywhere",
                  style: GoogleFonts.nothingYouCouldDo(
                      fontSize: 30, color: Colors.white),
                ),
              ],
            ),
          ),
          const SizedBox(height: 50),
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(70),
                  topRight: Radius.circular(70),
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(30),
                child: LoginMainWidget(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LoginMainWidget extends StatefulWidget {
  const LoginMainWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LoginMainWidgetState();
  }
}

class _LoginMainWidgetState extends State<LoginMainWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Gitlab.entries,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return LoginForm(
          tokenController: TextEditingController(
              text: snapshot.data[StringConstants.tokenSharedPreferences]),
          urlController: TextEditingController(
              text: snapshot.data[StringConstants.gitlabUrlSharedPreference]),
        );
      },
    );
  }
}
