import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:gitoune/gitlab/gitlab.dart';

class ReadMeViewer extends StatefulWidget {
  final String url;

  const ReadMeViewer({Key? key, required this.url}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ReadMeViewerState();
  }
}

class _ReadMeViewerState extends State<ReadMeViewer> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Gitlab().getRemoteContent(widget.url),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return const Center(child: CircularProgressIndicator());
          }

          return Column(children: [
            Markdown(
              data: snapshot.data!,
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
            )
          ]);
        });
  }
}
