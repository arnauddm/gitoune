import 'package:flutter/material.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/screens/app_layout.dart';
import 'package:gitoune/screens/readme/readme_viewer.dart';
import 'package:gitoune/screens/widgets/title.dart';

class ReadMeScreen extends StatelessWidget {
  final Project project;

  const ReadMeScreen({Key? key, required this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScreenTitle(
            title: "ReadMe",
            subTitle: project.nameWithNamespace,
          ),
          ReadMeViewer(url: project.readMeUrl),
        ],
      ),
    );
  }
}
