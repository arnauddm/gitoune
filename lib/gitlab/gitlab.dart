import 'dart:convert';
import 'package:gitoune/models/branch.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:gitoune/constants/string_constants.dart';
import 'package:gitoune/models/badge.dart';
import 'package:gitoune/models/commit.dart';
import 'package:gitoune/models/job.dart';
import 'package:gitoune/models/package.dart';
import 'package:gitoune/models/package_file.dart';
import 'package:gitoune/models/pipeline.dart';
import 'package:gitoune/models/project.dart';
import 'package:gitoune/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Gitlab {
  // static late String _token;

  static set login(String token) {
    SharedPreferences.getInstance().then((instance) =>
        instance.setString(StringConstants.tokenSharedPreferences, token));
  }

  static set configureBaseUrl(String url) {
    SharedPreferences.getInstance().then((instance) =>
        instance.setString(StringConstants.gitlabUrlSharedPreference, url));
  }

  static Future<Map<String, String>> get entries async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return {
      StringConstants.tokenSharedPreferences:
          prefs.getString(StringConstants.tokenSharedPreferences) ?? "",
      StringConstants.gitlabUrlSharedPreference:
          prefs.getString(StringConstants.gitlabUrlSharedPreference) ??
              "https://gitlab.com"
    };
  }

  final String baseUrl = "https://gitlab.com";

  Gitlab();

  Future<Map<String, String>> buildHeaders() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return buildHeadersWithToken(
        prefs.getString(StringConstants.tokenSharedPreferences) ?? "");
  }

  Map<String, String> buildHeadersWithToken(final String token) {
    return {"PRIVATE-TOKEN": token};
  }

  Future<String> getBaseUrl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return (prefs.getString(StringConstants.gitlabUrlSharedPreference) ??
        "https://gitlab.com");
  }

  Future<String> buildUri(String endpoint) async {
    return await getBaseUrl() + "/api/v4" + endpoint;
  }

  String paginationParameter(int itemCountToRetrieve) {
    return "&per_page=" + itemCountToRetrieve.toString();
  }

  Future<List<Project>> getProjects() async {
    var url = Uri.parse(await buildUri("/projects?membership=true"));

    var response = await http.get(url, headers: await buildHeaders());
    List parsedResp = json.decode(response.body);

    List<Project> projects = <Project>[];

    for (var element in parsedResp) {
      var prj = Project(
          id: element["id"] ?? -1,
          groupName: element["namespace"]["name"] ?? "",
          name: element["name"] ?? "",
          description: element["description"] ?? "",
          webUrl: element["web_url"] ?? "",
          readMeUrl: element["readme_url"] ?? "",
          tags: element["topics"] ?? [],
          startCount: element["star_count"] ?? 0,
          forkCount: element["forks_count"] ?? 0,
          nameWithNamespace: element["name_with_namespace"]);
      projects.add(prj);
    }

    return projects;
  }

  Future<User?> getUser(String userToken) async {
    var url = Uri.parse(await buildUri(("/user")));
    var response =
        await http.get(url, headers: buildHeadersWithToken(userToken));

    if (response.statusCode == 401) return null;

    Map parsedResponse = json.decode(response.body);

    return User(parsedResponse["username"] ?? "", parsedResponse["name"] ?? "",
        parsedResponse["email"] ?? "", parsedResponse["avatar_url"] ?? "");
  }

  Future<User?> getCurrentUser() async {
    Map userEntries = await entries;
    return await getUser(userEntries[StringConstants.tokenSharedPreferences]);
  }

  Future<List<Pipeline>> getPipelinesForProject(int projectId,
      {int itemCount = 100}) async {
    var url = Uri.parse(await buildUri("/projects/" +
        projectId.toString() +
        "/pipelines?order_by=updated_at" +
        paginationParameter(itemCount)));
    var response = await http.get(url, headers: await buildHeaders());

    List parsedResponse = json.decode(response.body);

    List<Pipeline> pipelines = [];
    for (var elt in parsedResponse) {
      Pipeline p = Pipeline(
          id: elt["id"],
          status: elt["status"],
          createdAt: DateTime.parse(elt["created_at"]),
          ref: elt["ref"],
          projectId: projectId,
          refSha: elt["sha"]);
      pipelines.add(p);
    }
    return pipelines;
  }

  Future<String> getRemoteContent(String uri) async {
    if (uri == "") return "";
    var response = await http.get(Uri.parse(uri.replaceAll("blob", "raw")),
        headers: await buildHeaders());
    return response.statusCode == 200 ? response.body : "";
  }

  Future<List<Badge>> getProjectBadge(int projectId) async {
    var url = Uri.parse(
        await buildUri(("/projects/" + projectId.toString() + "/badges")));
    var response = await http.get(url, headers: await buildHeaders());

    List parsedResponse = json.decode(response.body);

    List<Badge> badges = [];
    for (Map b in parsedResponse) {
      badges.add(Badge(b["id"], b["name"], b["image_url"]));
    }

    return badges;
  }

  Future<Map<String, List<Job>>> getJobs(int projectId, int pipelineId) async {
    var url = Uri.parse(await buildUri("/projects/" +
        projectId.toString() +
        "/pipelines/" +
        pipelineId.toString() +
        "/jobs"));

    var response = await http.get(url, headers: await buildHeaders());
    List parsedResponse = json.decode(response.body);

    Map<String, List<Job>> jobs = {};
    for (Map j in parsedResponse) {
      List<String> artifacts = [];

      for (var a in j["artifacts"]) {
        artifacts.add(a["filename"]);
      }

      var job = Job(
          id: j["id"],
          duration: j["duration"] ?? 0,
          name: j["name"],
          stage: j["stage"] ?? "",
          status: j["status"],
          tags: j["tag_list"],
          userAuthor: j["user"]["name"],
          artifacts: artifacts,
          ref: j["ref"],
          projectId: projectId,
          allowFailure: j["allow_failure"] ?? false);

      if (jobs.containsKey(job.stage)) {
        jobs[job.stage]!.add(job);
      } else {
        jobs.addAll({
          job.stage: [job]
        });
      }
    }
    return jobs;
  }

  Future<String> downloadArtifact(int projectId, String refName,
      String artifactName, String jobName) async {
    var url = Uri.parse(await buildUri("/projects/" +
        projectId.toString() +
        "/jobs/artifacts/" +
        refName +
        "/raw/" +
        artifactName +
        "?job=" +
        jobName));

    var response = await http.get(url, headers: await buildHeaders());
    return response.body;
  }

  Future<Commit> getCommit(int projectId, String commitSha) async {
    var url = Uri.parse(await buildUri("/projects/" +
        projectId.toString() +
        "/repository/commits/" +
        commitSha));
    var response = await http.get(url, headers: await buildHeaders());

    Map rsp = json.decode(response.body);

    return Commit(
      authorEmail: rsp["author_email"],
      authorName: rsp["author_name"],
      authoredTime: DateTime.parse(rsp["authored_date"]),
      committedDate: DateTime.parse(rsp["committed_date"]),
      committerName: rsp["committer_name"],
      createdAt: DateTime.parse(rsp["created_at"]),
      id: rsp["id"],
      shortId: rsp["short_id"],
      message: rsp["message"],
      title: rsp["title"],
    );
  }

  Future<Map<String, List<Package>>> getProjectPackages(
      final int projectId) async {
    var url = Uri.parse(
        await buildUri("/projects/" + projectId.toString() + "/packages"));
    var response = await http.get(url, headers: await buildHeaders());

    List parsedResponse = json.decode(response.body);
    Map<String, List<Package>> packages = {};

    for (var pkg in parsedResponse) {
      Package package = Package(
          id: pkg["id"],
          name: pkg["name"],
          version: pkg["version"],
          type: pkg["package_type"],
          createdAt: DateTime.parse(pkg["created_at"]));

      if (packages.containsKey(package.version)) {
        packages[package.version]!.add(package);
      } else {
        packages.addAll({
          package.version: [package]
        });
      }
    }

    return packages;
  }

  Future<List<PackageFile>> getPackageFiles(
      int projectId, int packageId) async {
    var url = Uri.parse(await buildUri("/projects/" +
        projectId.toString() +
        "/packages/" +
        packageId.toString() +
        "/package_files"));
    var response = await http.get(url, headers: await buildHeaders());

    List parsedResponse = json.decode(response.body);
    List<PackageFile> files = [];

    for (Map f in parsedResponse) {
      files.add(
        PackageFile(
            id: f["id"],
            packageId: f["package_id"],
            createdAt: DateTime.parse(f["created_at"]),
            fileMd5: f["file_md5"] ?? "",
            fileName: f["file_name"] ?? "",
            fileSha1: f["file_sha1"] ?? "",
            fileSha256: f["file_sha256"] ?? "",
            size: f["size"] ?? 0),
      );
    }

    return files;
  }

  void downloadPackage(String projectWebUrl, int packageFileId) async {
    var url = Uri.parse(projectWebUrl +
        "/-/package_files/" +
        packageFileId.toString() +
        "/download");

    launch(url.toString());
  }

  Future<List<Branch>> getBranches(final int projectId) async {
    var url = Uri.parse(await buildUri(
        "/projects/" + projectId.toString() + "/repository/branches"));
    var response = await http.get(url, headers: await buildHeaders());

    List parsedResponse = json.decode(response.body);
    List<Branch> branches = [];

    for (Map b in parsedResponse) {
      branches.add(
        Branch(
          name: b["name"],
          isMerged: b["merged"],
          canPush: b["can_push"],
          commit: Commit(
              id: b["commit"]["id"],
              shortId: b["commit"]["short_id"],
              title: b["commit"]["title"],
              message: b["commit"]["message"],
              authorEmail: b["commit"]["author_email"],
              createdAt: DateTime.parse(b["commit"]["created_at"]),
              authorName: b["commit"]["author_name"],
              authoredTime: DateTime.parse(b["commit"]["authored_date"]),
              committedDate: DateTime.parse(b["commit"]["committed_date"]),
              committerName: b["commit"]["committer_name"]),
          developersCanMerge: b["developers_can_merge"],
          developersCanPush: b["developers_can_push"],
          isProtected: b["protected"],
          webUrl: b["web_url"],
          isDefaultBranch: b["default"],
        ),
      );
    }

    return branches;
  }
}
