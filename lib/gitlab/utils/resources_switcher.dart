import 'package:flutter/material.dart' show Color, IconData, Icons;
import 'package:gitoune/constants/color_constants.dart';

class ResourcesSwitcher {
  static IconData getIcon(final String status,
      {final bool allowFailure = false}) {
    switch (status) {
      case "pending":
        return Icons.pending;
      case "created":
        return Icons.add_circle;
      case "running":
        return Icons.run_circle;
      case "success":
        return Icons.check_circle;
      case "failed":
        return allowFailure ? Icons.warning : Icons.error;
      default:
        return Icons.help;
    }
  }

  static Color getColor(final String status,
      {final bool allowFailure = false}) {
    switch (status) {
      case "pending":
      case "created":
        return ColorConstants.pipelinePending;
      case "running":
        return ColorConstants.pipelineRunning;
      case "success":
        return ColorConstants.pipelineSuccess;
      case "failed":
        return allowFailure
            ? ColorConstants.pipelineAllowFailure
            : ColorConstants.pipelineFailed;
      default:
        return ColorConstants.pipelineDefault;
    }
  }
}
