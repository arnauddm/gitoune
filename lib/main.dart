import 'package:flutter/material.dart';
import 'package:gitoune/screens/login/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application. 110 150 244
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gitoune',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}
